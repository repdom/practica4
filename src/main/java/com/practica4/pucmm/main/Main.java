package com.practica4.pucmm.main;

import com.practica4.pucmm.entidades.*;
import com.practica4.pucmm.services.*;
import freemarker.template.Configuration;
import org.jasypt.util.text.StrongTextEncryptor;
import spark.Request;
import spark.Response;
import spark.template.freemarker.FreeMarkerEngine;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import java.sql.Array;
import java.sql.Date;
import java.time.LocalDate;
import java.util.*;

import static spark.Spark.*;
import static spark.debug.DebugScreen.enableDebugScreen;

public class Main {
    private static String contrasenia = "practica3";

    public static void main(String[] args) {
        DbService.getInstancia().iniciarDn();
        Configuration configuration=new Configuration(Configuration.getVersion());
        configuration.setClassForTemplateLoading(Main.class, "/templates");
        FreeMarkerEngine freeMarkerEngine = new FreeMarkerEngine(configuration);
        crearEntidades();
        enableDebugScreen();

        get("/", (request, response) -> {
            boolean logged = estaLogueado(request);
            Map<String, Object> attributes = new HashMap<>();
            Set<Articulo> articulos = ArticuloService.getInstancia().findAllDescByFecha();
            attributes.put("estaLogueado", logged);
            attributes.put("paginas", listaEntera(articulos.size()/2));
            Set<Articulo>artAuxList = ArticuloService.getInstancia().findAllbyPagination(2, 1);
            attributes.put("articulosPaginados", artAuxList);
            if (artAuxList.size() > 0) {
                attributes.put("primerArticulo", artAuxList.toArray()[0]);
            }
            return modelAndView(attributes, "index.ftl");
        }, freeMarkerEngine);
        get("/:numeroPagina",(request, response) -> {
            boolean logged = estaLogueado(request);
            int numeroPagina = Integer.valueOf(request.params("numeroPagina"));
            Map<String, Object> attributes = new HashMap<>();
            Set<Articulo> articulos = ArticuloService.getInstancia().findAllDescByFecha();
            attributes.put("estaLogueado", logged);
            attributes.put("articulos", articulos);
            attributes.put("paginas", listaEntera(articulos.size()/2));
            Set<Articulo>artAuxList = ArticuloService.getInstancia().findAllbyPagination(2, numeroPagina);
            attributes.put("articulosPaginados", artAuxList);
            if (artAuxList.size() > 0) {
                attributes.put("primerArticulo", artAuxList.toArray()[0]);
            }
            return modelAndView(attributes, "index.ftl");
        }, freeMarkerEngine);

        //parte login de la aplicación
        get("/login/", (request, response) -> {
            boolean logged = estaLogueado(request);
            if(logged == true) {
                response.redirect("/");
            }
            return modelAndView(null, "login.ftl");
        }, freeMarkerEngine);
        post("/loguearse/", (request, response) -> {
            boolean logged = estaLogueado(request);
            if(logged == true) {
                response.redirect("/");
            }
            Usuario usuario = obtenerUsuario(String.valueOf(request.queryParams("usuario")));
            if(usuario != null) {
                response = guardarUsuarioEncriptado(usuario, response, request);
            }
            response.redirect("/");
            return null;
        }, freeMarkerEngine);

        //parte de articulos
        get("/articulo/", (request, response) -> {
            boolean estaLogueado = estaLogueado(request);
            boolean esAutor = revisarCookie(request);
            if (!esAutor && !estaLogueado) {
                response.redirect("/");
            }
            return modelAndView(null, "crearArticulo.ftl");
        }, freeMarkerEngine);
        post("/articulo/agregarArticulo/", (request, response) -> {

            boolean estaLogueado = estaLogueado(request);
            boolean esAutor = revisarCookie(request);
            if (!esAutor && !estaLogueado) {
                response.redirect("/");
            }
            guardarArticulo(request);
            response.redirect("/");
            return null;
        }, freeMarkerEngine);
        get("/articulo/:codigoArticulo", (request, response) -> {
            Long codigoArticulo = Long.valueOf(request.params("codigoArticulo").replaceAll(",", ""));
            Articulo articulo = ArticuloService.getInstancia().find(codigoArticulo);
            StrongTextEncryptor textEncryptor = new StrongTextEncryptor();
            textEncryptor.setPassword(contrasenia);
            Map<String, Object> attributes = new HashMap<>();
            attributes.put("articulo", articulo);
            attributes.put("esAdministrador", Boolean.valueOf(textEncryptor.decrypt(request.cookie("esAdministrador"))));
            attributes.put("esAutor", Boolean.valueOf(textEncryptor.decrypt(request.cookie("esAutor"))));
            attributes.put("usuario", (textEncryptor.decrypt(request.cookie("usuario")) != null)?true:false);
            attributes.put("esMismoAutor", (articulo.getUsuario().getUsuario() == textEncryptor.decrypt(request.cookie("usuario"))));
            attributes.put("cantLike", articulo.getListaLike().size());
            attributes.put("cantDislike", articulo.getListaDislike().size());
            return modelAndView(attributes, "articulo.ftl");
        }, freeMarkerEngine);
        get("/articulo/like/:codigoArticulo", (request, response) -> {
            if(!estaLogueado(request)) {
                response.redirect("/articulo/"+request.params("codigoArticulo").replaceAll(",", ""));
                return null;
            }
            Long codigoArticulo = Long.valueOf(request.params("codigoArticulo").replaceAll(",", ""));
            Articulo articulo = ArticuloService.getInstancia().find(codigoArticulo);
            Like like = new Like();
            like.setArticuloLike(articulo);
            like.setUsuarioLike(obtenerUsuario(getStrongPassword().decrypt(request.cookie("usuario"))));
            MegustaService.getInstancia().crear(like);
            response.redirect("/articulo/" + codigoArticulo);
            return null;
        }, freeMarkerEngine);
        get("/articulo/dislike/:codigoArticulo", (request, response) -> {
            Long codigoArticulo = Long.valueOf(request.params("codigoArticulo").replaceAll(",", ""));
            if(!estaLogueado(request)) {
                response.redirect("/articulo/"+request.params("codigoArticulo").replaceAll(",", ""));
                return null;
            }
            Articulo articulo = ArticuloService.getInstancia().find(codigoArticulo);
            Dislike dislike = new Dislike();
            dislike.setArticulo(articulo);
            dislike.setUsuario(obtenerUsuario(getStrongPassword().decrypt(request.cookie("usuario"))));
            NoMeGustaService.getInstancia().crear(dislike);
            response.redirect("/articulo/" + codigoArticulo);
            return null;
        }, freeMarkerEngine);
        get("/editarArticulo/:codigoArticulo", (request, response) -> {
            boolean esAdminOesAutor = revisarCookie(request);
            Long codigoArticulo = Long.valueOf(request.params("codigoArticulo").replaceAll(",", ""));
            Articulo articulo = ArticuloService.getInstancia().find(codigoArticulo);
            Map<String, Object> attributes = new HashMap<>();
            if (!esAdminOesAutor) {
                response.redirect("/articulo/" + request.params("codigoArticulo").replaceAll(",", ""));
            }
            String etiquetaString = "";
            for (Etiqueta etq: articulo.getListaEtiquetas()) {
                etiquetaString += etq.getNombre() + ",";
            }
            attributes.put("etiquetaString", etiquetaString);
            attributes.put("articulo", articulo);
            return modelAndView(attributes, "editarArticulo.ftl");
        }, freeMarkerEngine);
        post("/agregarEdicion/:codigoArticulo", (request, response) -> {
            boolean esAdminOesAutor = revisarCookie(request);
            Articulo articulo = ArticuloService.getInstancia().find(Long.valueOf(request.params("codigoArticulo").replaceAll(",", "")));
            if (!esAdminOesAutor) {
                response.redirect("/articulo/" + request.params("codigoArticulo"));
            }
            editarArticulo(request, articulo);
            response.redirect("/articulo/" + request.params("codigoArticulo").replaceAll(",", ""));
            return null;
        }, freeMarkerEngine);
        get("/eliminarArticulo/:codigoArticulo", (request, response) -> {
            boolean esAdminOesAutor = revisarCookie(request);
            boolean estaLogueado = estaLogueado(request);
            long codigo = Long.valueOf(request.params("codigoArticulo").replaceAll(",", ""));

            if (!esAdminOesAutor && estaLogueado) {
                response.redirect("/articulo/" + request.params("codigoArticulo").replaceAll(",", ""));
            }
            Articulo articuloAux = ArticuloService.getInstancia().find(codigo);
            ArticuloService.getInstancia().eliminar(codigo);
            response.redirect("/");
            return null;
        }, freeMarkerEngine);
        // apartado de comentarios
        post("/comentar/:codigoArticulo", (request, response) -> {
            Long codigo = Long.valueOf(request.params("codigoArticulo").replaceAll(",", ""));
            Articulo articulo = ArticuloService.getInstancia().find(codigo);
            String comentario = request.queryParams("comentario");
            StrongTextEncryptor textEncryptor = new StrongTextEncryptor();
            textEncryptor.setPassword(contrasenia);
            String usuario = textEncryptor.decrypt(request.cookie("usuario"));
            Usuario user = obtenerUsuario(usuario);
            if (usuario != null) {
                Comentario coment = new Comentario();
                coment.setComentario(comentario);
                coment.setUsuario(user);
                coment.setArticulo(articulo);
                ComentarioService.getInstancia().crear(coment);
                response.redirect("/articulo/" + String.valueOf(codigo).replaceAll(",", ""));
            }
            response.redirect("/articulo/" + String.valueOf(codigo).replaceAll(",", ""));
            return null;
        }, freeMarkerEngine);
        get("/comentario/like/:comentarioCodigo", (request, response) -> {
            Long codigo = Long.valueOf(request.params("comentarioCodigo").replaceAll(",", ""));
            Comentario comentario = ComentarioService.getInstancia().find(codigo);
            if (!estaLogueado(request)) {
                response.redirect("/articulo/"+comentario.getArticulo().getCodigo());
                return null;
            }
            comentario.setMeGusta(comentario.getMeGusta() + 1);
            ComentarioService.getInstancia().editar(comentario);
            response.redirect("/articulo/"+comentario.getArticulo().getCodigo());
            return null;
        }, freeMarkerEngine);
        get("/comentario/dislike/:comentarioCodigo", (request, response) -> {
            Long codigo = Long.valueOf(request.params("comentarioCodigo").replaceAll(",", ""));
            if (!estaLogueado(request)) {
                response.redirect("/articulo/"+ComentarioService.getInstancia().find(codigo).getArticulo().getCodigo());
                return null;
            }
            Comentario comentario = crearNoMegusta(ComentarioService.getInstancia().find(codigo));
            response.redirect("/articulo/"+comentario.getArticulo().getCodigo());
            return null;
        }, freeMarkerEngine);
        get("/eliminarComentario/:codigoComentario/:codigoArticulo", (request, response) -> {
            Long codigoArticulo = Long.valueOf(request.params("codigoArticulo").replaceAll(",", ""));
            Long codigoComentario = Long.valueOf(request.params("codigoComentario").replaceAll(",", ""));
            boolean esAdminOesAutor = revisarCookie(request);
            if (!esAdminOesAutor) {
                response.redirect("/articulo/" + request.params("codigoArticulo").replaceAll(",", ""));
            }
            ComentarioService.getInstancia().eliminar(codigoComentario);
            response.redirect("/articulo/" + String.valueOf(codigoArticulo).replaceAll(",", ""));
            return null;
        }, freeMarkerEngine);
        get("/logout/", (request, response) -> {
            String usuario = request.cookie("usuario");
            String esAdmin = request.cookie("esAdministrador");
            String esAutor = request.cookie("esAutor");

            response.removeCookie("/", "usuario");
            response.removeCookie("/","esAdministrador");
            response.removeCookie("/","esAutor");
            response.redirect("/");
            return null;
        }, freeMarkerEngine);
        get("/articulosPorEtiqueta/:codigoEtiqueta", (request, response) -> {
            int codigo = Integer.valueOf(request.params("codigoEtiqueta"));
            Etiqueta etiqueta = EtiquetaService.getInstancia().find(codigo);
            Map<String, Object> attributes = new HashMap<>();
            attributes.put("etiqueta", etiqueta);
            attributes.put("usuario", (getStrongPassword().decrypt(request.cookie("usuario")) != null)?true:false);
            return modelAndView(attributes, "listaArticulosEtiqueta.ftl");
        }, freeMarkerEngine);
        /*get("/:paginaArticulo", (request, response) -> {

        }, freeMarkerEngine);*/
    }

    static private Set<Integer> listaEntera(int tam) {
        Set<Integer> cantArticulos = new HashSet<>();
        for(int i = 1; i <= Math.ceil(tam)+1; i++) {
            cantArticulos.add(i);
        }
        return cantArticulos;
    }
    private static Comentario crearNoMegusta(Comentario comentario) {
        comentario.setNoMegusta(comentario.getNoMegusta()+1);
        ComentarioService.getInstancia().editar(comentario);
        return comentario;
    }
    private static void crearEntidades() {
        EntityManagerFactory emf =  Persistence.createEntityManagerFactory("MiUnidadPersistencia");
        EntityManager entityManager = emf.createEntityManager();
        entityManager.getTransaction().begin();

        Usuario usuario = new Usuario("repdom", "admin", "RDsIdEe1865@", true, true);
        UsuarioService.getInstancia().crear(usuario);
        //entityManager.persist(usuario);
        // entityManager.getTransaction().commit();
    }
    private static boolean revisarCookie(Request request) {
        StrongTextEncryptor textEncryptor = new StrongTextEncryptor();
        textEncryptor.setPassword(contrasenia);
        String usuario = textEncryptor.decrypt(request.cookie("usuario"));
        boolean esAdministrador = Boolean.valueOf(textEncryptor.decrypt(request.cookie("esAdministrador")));
        boolean esAutor = Boolean.valueOf(textEncryptor.decrypt(request.cookie("esAutor")));
        if (!esAutor || !esAdministrador) {
            return false;
        }
        return true;
    }
    private static boolean estaLogueado(Request request) {
        StrongTextEncryptor textEncryptor = new StrongTextEncryptor();
        textEncryptor.setPassword(contrasenia);

        String usuario = textEncryptor.decrypt(request.cookie("usuario"));
        String password = textEncryptor.decrypt(request.cookie("contrasenia"));

        if(usuario == null && password == null) {
           return false;
        }
        return true;
    }
    private static Usuario obtenerUsuario(String username) {
        Usuario usuario = UsuarioService.getInstancia().find(username);
        return usuario;
    }
    private static Response guardarUsuarioEncriptado(Usuario usuario, Response response, Request request) {
        StrongTextEncryptor textEncryptor = new StrongTextEncryptor();
        textEncryptor.setPassword(contrasenia);
        String userEncripted = textEncryptor.encrypt(usuario.getUsuario());
        String userAdmin = textEncryptor.encrypt(String.valueOf(usuario.isAdministrador()));
        String userAutor = textEncryptor.encrypt(String.valueOf(usuario.isAutor()));
        int esMantenerSesion = (request.queryParams("colocarCookieDeMantenerSesion") != null?86400 * 7:1000);
        response.cookie("/", "usuario", userEncripted, esMantenerSesion, false);
        response.cookie("/", "esAdministrador", userAdmin, esMantenerSesion, false);
        response.cookie("/", "esAutor", userAutor, esMantenerSesion, false);
        return response;
    }
    private static Set<Etiqueta> filtrarEtiqueta(String [] etiquetasDiv, Articulo articulo) {
        Set<Etiqueta>etiquetasLista = new HashSet<>();
        for (String etiqueta: etiquetasDiv) {
            //  revisando si la etiqueta existe
            Etiqueta etiquetaExistente = EtiquetaService.getInstancia().findSegunColumna(Arrays.asList("nombre"), Arrays.asList(etiqueta));
            if (etiquetaExistente != null) {
                etiquetasLista.add(etiquetaExistente);
            } else {
                Etiqueta etiquetaAux = new Etiqueta();
                etiquetaAux.setNombre(etiqueta);
                Set<Articulo> articuloEtiqueta = new HashSet<>();
                articuloEtiqueta.add(articulo);
                etiquetaAux.setArticulo(articuloEtiqueta);
                etiquetasLista.add(etiquetaAux);
            }
        }
        return etiquetasLista;
    }
    private static boolean guardarArticulo(Request request) {
        StrongTextEncryptor textEncryptor = new StrongTextEncryptor();
        textEncryptor.setPassword(contrasenia);
        String usuario = textEncryptor.decrypt(request.cookie("usuario"));

        Articulo articulo = new Articulo();
        Usuario usuarioDb = obtenerUsuario(usuario);
        articulo.setUsuario(usuarioDb);
        articulo.setFecha_publicacion(Date.valueOf(LocalDate.now()));
        articulo.setTitulo(request.queryParams("titulo"));
        articulo.setCuerpo(request.queryParams("cuerpoArticulo"));

        Set<Etiqueta>etiquetasLista = new HashSet<>();

        String etiquetas = request.queryParams("etiquetas");
        String etiquetasDiv[] = etiquetas.split(",");
        etiquetasLista = filtrarEtiqueta(etiquetasDiv, articulo);
        articulo.setListaEtiquetas(etiquetasLista);
        ArticuloService.getInstancia().crear(articulo);
        for(Etiqueta etiquetaInstance: articulo.getListaEtiquetas()) {
            Etiqueta etiquetaExistente = EtiquetaService.getInstancia().findSegunColumna(Arrays.asList("nombre"), Arrays.asList(etiquetaInstance.getNombre()));
            if (etiquetaExistente == null) {
                EtiquetaService.getInstancia().crear(etiquetaInstance);
            } else {
                etiquetaExistente.getArticulo().add(articulo);
                EtiquetaService.getInstancia().editar(etiquetaExistente);
            }
        }
        return true;
    }
    private static StrongTextEncryptor getStrongPassword() {
        StrongTextEncryptor textEncryptor = new StrongTextEncryptor();
        textEncryptor.setPassword(contrasenia);
        return textEncryptor;
    }
    private static boolean editarArticulo(Request request, Articulo articulo) {
        articulo.setTitulo(request.queryParams("titulo"));
        articulo.setCuerpo(request.queryParams("cuerpoArticulo"));

        Set<Etiqueta>listaEtiqueta = new HashSet<>();
        String etiquetasParams = request.queryParams("etiquetas");
        String etiquetasDiv[] = etiquetasParams.split(",");
        listaEtiqueta = filtrarEtiqueta(etiquetasDiv, articulo);
        for (Etiqueta et: articulo.getListaEtiquetas()) {
            boolean incluye = true;
            for (Etiqueta etq: listaEtiqueta) {
                if(etq.getNombre() == et.getNombre()) {
                    incluye = false;
                }
            }
            if(incluye == true) {
                et.getArticulo().remove(articulo);
                EtiquetaService.getInstancia().editar(et);
            }
        }
        for (Etiqueta etq : listaEtiqueta) {
            Etiqueta etiquetaExiste = EtiquetaService.getInstancia().findSegunColumna(Arrays.asList("nombre"), Arrays.asList(etq.getNombre()));
            if(etiquetaExiste == null){
                EtiquetaService.getInstancia().crear(etq);
            }else{
                boolean incluye = true;
                for (Etiqueta et: articulo.getListaEtiquetas()) {
                    if(et.getNombre() == etq.getNombre()) {
                        incluye = false;
                    }
                }
                if(incluye == true) {
                    etq.getArticulo().add(articulo);
                    EtiquetaService.getInstancia().editar(etq);
                }
            }
        }
        articulo.setListaEtiquetas(listaEtiqueta);
        ArticuloService.getInstancia().editar(articulo);
        return true;
    }
}

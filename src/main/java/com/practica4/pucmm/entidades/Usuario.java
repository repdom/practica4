package com.practica4.pucmm.entidades;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
public class Usuario implements Serializable {
    @Id
    private String usuario;
    private String nombre;
    private String Contrasenia;
    private boolean administrador;
    private boolean autor;

    @OneToMany(mappedBy = "usuario", fetch = FetchType.EAGER)
    private Set<Comentario>listaComentarios;

    @OneToMany(mappedBy = "usuario", fetch = FetchType.EAGER)
    private Set<Articulo> listaArticulos;

    @OneToMany(mappedBy = "usuarioLike", fetch = FetchType.EAGER)
    private Set<Like> listaLike;

    @OneToMany(mappedBy = "usuario")
    private Set<Dislike> disLikes;

    public Usuario() {

    }

    public Usuario(String usuario, String nombre, String contrasenia, boolean administrador, boolean autor) {
        this.usuario = usuario;
        this.nombre = nombre;
        this.Contrasenia = contrasenia;
        this.administrador = administrador;
        this.autor = autor;
    }
    public String getUsuario() {
        return usuario;
    }
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContrasenia() {
        return Contrasenia;
    }

    public void setContrasenia(String contrasenia) {
        Contrasenia = contrasenia;
    }

    public boolean isAdministrador() {
        return administrador;
    }

    public void setAdministrador(boolean administrador) {
        this.administrador = administrador;
    }

    public boolean isAutor() {
        return autor;
    }

    public void setAutor(boolean autor) {
        this.autor = autor;
    }

    public Set<Comentario> getListaComentarios() {
        return listaComentarios;
    }

    public void setListaComentarios(Set<Comentario> listaComentarios) {
        this.listaComentarios = listaComentarios;
    }

    public Set<Articulo> getListaArticulos() {
        return listaArticulos;
    }

    public void setListaArticulos(Set<Articulo> listaArticulos) {
        this.listaArticulos = listaArticulos;
    }
    public Set<Like> getListaLike() {
        return listaLike;
    }
    public void setListaLike(Set<Like> listaLike) {
        this.listaLike = listaLike;
    }
    public Set<Dislike> getDisLikes() {
        return disLikes;
    }

    public void setDisLikes(Set<Dislike> disLikes) {
        this.disLikes = disLikes;
    }

}

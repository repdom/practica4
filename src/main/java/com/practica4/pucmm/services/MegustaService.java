package com.practica4.pucmm.services;

import com.practica4.pucmm.entidades.Like;

public class MegustaService extends GeneralService {
    private static MegustaService megustaServiceInstance;

    public MegustaService() {
        super(Like.class);
    }

    public static MegustaService getInstancia() {
        if (megustaServiceInstance == null) {
            megustaServiceInstance = new MegustaService();
        }
        return megustaServiceInstance;
    }

}

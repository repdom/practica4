package com.practica4.pucmm.services;

import com.practica4.pucmm.entidades.Etiqueta;

public class EtiquetaService extends GeneralService<Etiqueta> {
    private static EtiquetaService etiquetaServiceIntance;

    public EtiquetaService() {
        super(Etiqueta.class);
    }

    public static EtiquetaService getInstancia() {
        if(etiquetaServiceIntance==null){
            etiquetaServiceIntance = new EtiquetaService();
        }
        return etiquetaServiceIntance;
    }

}

package com.practica4.pucmm.services;

import com.practica4.pucmm.entidades.Comentario;

public class ComentarioService extends GeneralService<Comentario> {
    private static ComentarioService comentarioServiceInstance;

    public ComentarioService() {
        super(Comentario.class);
    }
    public static ComentarioService getInstancia() {
        if(comentarioServiceInstance==null){
            comentarioServiceInstance = new ComentarioService();
        }
        return comentarioServiceInstance;
    }


}

package com.practica4.pucmm.services;

import com.practica4.pucmm.entidades.Usuario;

public class UsuarioService extends GeneralService<Usuario> {
    private static UsuarioService usuarioServiceInstance;

    public UsuarioService() {
        super(Usuario.class);
    }
    public static UsuarioService getInstancia(){
        if(usuarioServiceInstance==null){
            usuarioServiceInstance = new UsuarioService();
        }
        return usuarioServiceInstance;
    }

}

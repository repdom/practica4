package com.practica4.pucmm.services;

import com.practica4.pucmm.entidades.Dislike;

public class NoMeGustaService extends GeneralService {
    private static NoMeGustaService noMegustaServiceInstance;

    public NoMeGustaService() {
        super(Dislike.class);
    }

    public static NoMeGustaService getInstancia() {
        if (noMegustaServiceInstance == null) {
            noMegustaServiceInstance = new NoMeGustaService();
        }
        return noMegustaServiceInstance;
    }

}

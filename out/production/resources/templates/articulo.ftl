<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="canonical" href="https://getbootstrap.com/docs/4.3/examples/blog/">

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <title>Articulo</title>
</head>
<body>
<div class="container">
    <#if usuario == false>
        <#include "/navbar.ftl">
    </#if>
    <#if usuario != false>
        <#include "/navbarlogged.ftl">
    </#if>
</div>
<div class="container">
    <h1 class="display-4">${articulo.getTitulo()}</h1>
    <p class="lead">${articulo.getCuerpo()}</p>
    <hr class="my-4">
    <p>${articulo.getUsuario().getUsuario()} | ${articulo.getFecha_publicacion()}</p>
    <#list articulo.getListaEtiquetas() as etiqueta>
        <a href="/articulosPorEtiqueta/${etiqueta.getCodigo()}">
            <button type="button" class="btn btn-outline-secondary">${etiqueta.getNombre()}</button>
        </a>
    </#list>
    <div class="container">
        <div class="cell"><a href="/articulo/like/${articulo.getCodigo()}"><button type="button" class="btn btn-outline-secondary">${cantLike} Me gusta</button></a>
            <a href="/articulo/dislike/${articulo.getCodigo()}"><button type="button" class="btn btn-danger">${cantDislike} No me gusta</button></a></div>
    </div>
    <#if esAdministrador == true || esMismoAutor == true>
        <a href="/editarArticulo/${articulo.getCodigo()}"><button type="button" class="btn btn-primary btn-lg btn-block">EDITAR</button></a>
        <a href="/eliminarArticulo/${articulo.getCodigo()}"><button type="button" class="btn btn-danger btn-lg btn-block">ELIMINAR</button></a>
    </#if>
    <#if usuario == true>
        <form action="/comentar/${articulo.getCodigo()}" method="post">
            <div class="form-group">
                <label for="comentario">Comentario</label>
                <textarea class="form-control" id="comentario" name="comentario" rows="5"></textarea>
                <button type="submit" class="btn btn-secondary btn-lg btn-block">COMENTAR</button>
            </div>
        </form>
    </#if>
    <#list articulo.getListaComentarios() as coment>
        <div class="card text-white bg-dark mb-3" style="max-width: 100%;">
            <div class="card-header"></div>
            <div class="card-body">
                <h5 class="card-title">${coment.getUsuario().getUsuario()}</h5>
                <p class="card-text">${coment.getComentario()}</p>
                <#if esAdministrador == true>
                    <a href="/eliminarComentario/${coment.getCodigo()}/${articulo.getCodigo()}"><button type="button" class="btn btn-danger btn-lg btn-block">ELIMINAR</button></a>
                </#if>
                <div class="container">
                    <div class="cell"><a href="/comentario/like/${coment.getCodigo()}"><button type="button" class="btn btn-outline-secondary">${coment.getMeGusta()} Me gusta</button></a>
                        <a href="/comentario/dislike/${coment.getCodigo()}"><button type="button" class="btn btn-danger">${coment.getNoMegusta()} No me gusta</button></a></div>
                </div>
            </div>
        </div>
    </#list>
</div>
</body>
</html>